package edu.nwmissouri.s521699.northwestacmadmin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.parse.SendCallback;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Date;
import java.util.Locale;

import edu.nwmissouri.s521699.northwestacmadmin.dialogs.DatePickerDialogFragment;
import edu.nwmissouri.s521699.northwestacmadmin.dialogs.TimePickerDialogFragment;

public class AddEventActivity extends AppCompatActivity implements DatePickerDialogFragment.DateCallback, TimePickerDialogFragment.TimeCallback {

    private EditText eventNameET;
    private EditText eventDescriptionET;
    private EditText eventLocationET;

    private TextView eventDateTV;
    private TextView eventTimeTV;

    private CheckBox programmingContestCB;
    private CheckBox webDesignCB;
    private CheckBox openSource;
    private CheckBox gameDevelopmentCB;
    private CheckBox dataScienceCB;
    private CheckBox androidCB;
    private CheckBox otherCB;

    private String dateString;

    HashMap<Integer, String> monthMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        assert getSupportActionBar() != null;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
        actionBar.setTitle("");

        this.monthMap = new HashMap<>();
        monthMap.put(0, "Jan");
        monthMap.put(1, "Feb");
        monthMap.put(2, "Mar");
        monthMap.put(3, "Apr");
        monthMap.put(4, "May");
        monthMap.put(5, "Jun");
        monthMap.put(6, "Jul");
        monthMap.put(7, "Aug");
        monthMap.put(8, "Sep");
        monthMap.put(9, "Oct");
        monthMap.put(10, "Nov");
        monthMap.put(11, "Dec");

        eventNameET = (EditText) findViewById(R.id.eventNameET);
        eventDescriptionET = (EditText) findViewById(R.id.agendaET);
        eventLocationET = (EditText) findViewById(R.id.locationET);

        eventDateTV = (TextView) findViewById(R.id.datePV);
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE", Locale.US);
        Date date = new Date(year, month, day - 1);
        String dayOfWeek = simpledateformat.format(date);
        eventDateTV.setText(dayOfWeek + ", " + monthMap.get(month) + " " + day + ", " + year);
        this.dateString = month + "/" + day + "/" + year;

        eventTimeTV = (TextView) findViewById(R.id.timePV);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        String minute = String.format("%02d", c.get(Calendar.MINUTE));
        if (hour > 12) {
            eventTimeTV.setText(hour - 12 + ":" + minute + " PM");
        } else {
            eventTimeTV.setText(hour + ":" + minute + " AM");
        }

        eventDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.show(getFragmentManager(), "Tag1");

            }
        });

        eventTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timePickerDialogFragment = new TimePickerDialogFragment();
                timePickerDialogFragment.show(getFragmentManager(), "Tag2");
            }
        });

        programmingContestCB = (CheckBox) findViewById(R.id.programmingContestsCB);
        webDesignCB = (CheckBox) findViewById(R.id.webDesignCB);
        openSource = (CheckBox) findViewById(R.id.openSourceCB);
        gameDevelopmentCB = (CheckBox) findViewById(R.id.gameDevelopmentCB);
        dataScienceCB = (CheckBox) findViewById(R.id.dataScienceCB);
        androidCB = (CheckBox) findViewById(R.id.androidCB);
        otherCB = (CheckBox) findViewById(R.id.otherCB);

    }


    @Override
    public void onDateSelect(String dateStr) {
        String[] dateParameters = dateStr.split("/");

        int monthOfYear = Integer.parseInt(dateParameters[0]);
        int dayOfMonth = Integer.parseInt(dateParameters[1]);
        int year = Integer.parseInt(dateParameters[2]);

        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEE", Locale.US);
        Date date = new Date(year, monthOfYear - 1, dayOfMonth - 1);
        String dayOfWeek = simpledateformat.format(date);

        this.eventDateTV.setText(dayOfWeek + ", " + monthMap.get(monthOfYear - 1) + " " + dayOfMonth + ", " + year);
        this.dateString = dateStr;
    }

    @Override
    public void onTimeSelect(String time) {
        ((TextView) findViewById(R.id.timePV)).setText(time);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_event_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.saveActionBTN) {

            if (eventNameET.getText().toString().trim().equals("") || eventDescriptionET.getText().toString().trim().equals("") || eventLocationET.getText().toString().trim().equals("")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Please enter all fields.");

                builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            } else {

                int month = Integer.parseInt((dateString.split("/"))[0]);
                int day = Integer.parseInt((dateString.split("/"))[1]);
                int year = Integer.parseInt((dateString.split("/"))[2]);
                int hour = Integer.parseInt(eventTimeTV.getText().toString().split(":")[0]);
                int minute = Integer.parseInt(eventTimeTV.getText().toString().split(":")[1].split(" ")[0]);

                Calendar c = Calendar.getInstance();

                c.set(year, month - 1, day, hour, minute);

                int sigGroupsCount = 0;
                ParseObject eventObject = new ParseObject("Events");
                eventObject.put("eventName", this.eventNameET.getText().toString());
                eventObject.put("eventDescription", this.eventDescriptionET.getText().toString());
                eventObject.put("eventLocation", this.eventLocationET.getText().toString());
                eventObject.put("eventDate", this.dateString);
                eventObject.put("sortingDate", c.getTime());

                eventObject.put("eventTime", this.eventTimeTV.getText().toString());
                ArrayList<String> sigList = new ArrayList<>();
                if (programmingContestCB.isChecked()) {
                    sigList.add("P");
                    sigGroupsCount++;
                }
                if (webDesignCB.isChecked()) {
                    sigList.add("W");
                    sigGroupsCount++;
                }
                if (openSource.isChecked()) {
                    sigList.add("O");
                    sigGroupsCount++;
                }
                if (gameDevelopmentCB.isChecked()) {
                    sigList.add("G");
                    sigGroupsCount++;
                }
                if (dataScienceCB.isChecked()) {
                    sigList.add("D");
                    sigGroupsCount++;
                }
                if (androidCB.isChecked()) {
                    sigList.add("A");
                    sigGroupsCount++;
                }
                if (otherCB.isChecked()) {
                    sigList.add("T");
                    sigGroupsCount++;
                }
                if (sigGroupsCount == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Please select at least one sig group.");

                    builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                } else {
                    eventObject.addAllUnique("sigGroups", sigList);

                    eventObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            ParsePush push = new ParsePush();
                            push.setChannel("Events");
                            push.setMessage("New event is posted.");
                            push.sendInBackground(new SendCallback() {
                                @Override
                                public void done(ParseException e) {
                                    Toast.makeText(AddEventActivity.this, "Notified all users", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                    finish();
                }
            }

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
