package edu.nwmissouri.s521699.northwestacmadmin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.parse.ParseObject;

import java.util.regex.Pattern;

public class AddPerson extends AppCompatActivity {

    private EditText personNameET;
    private EditText roleET;
    private EditText phoneNumberET;
    private EditText emailET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);

        assert getSupportActionBar() != null;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
        actionBar.setTitle("");

        personNameET = (EditText) findViewById(R.id.personNameET);
        roleET = (EditText) findViewById(R.id.roleET);
        phoneNumberET = (EditText) findViewById(R.id.phoneNumberET);
        emailET = (EditText) findViewById(R.id.emailET);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_event_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        if (id == R.id.saveActionBTN) {

            if (personNameET.getText().toString().trim().equals("") ||
                    roleET.getText().toString().trim().equals("") ||
                    phoneNumberET.getText().toString().trim().equals("") ||
                    emailET.getText().toString().trim().equals("")) {

                showAlertDialog(R.string.empty_fields_error_message);
            } else if (!validateEmail(emailET.getText().toString())) {
                showAlertDialog(R.string.email_address_error_message);
            } else if (!validatePhoneNumber(phoneNumberET.getText().toString())) {
                showAlertDialog(R.string.phone_number_error_message);
            } else {

                ParseObject personObject = new ParseObject("Persons");
                personObject.put("personName", personNameET.getText().toString());
                personObject.put("personRole", roleET.getText().toString());
                personObject.put("personPhoneNum", phoneNumberET.getText().toString());
                personObject.put("personEmail", emailET.getText().toString());
                personObject.saveInBackground();

                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean validateEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public boolean validatePhoneNumber(String phoneNumber) {
        if (phoneNumber.length() != 10) {
            return false;
        } else {
            Pattern pattern = Patterns.PHONE;
            return pattern.matcher(phoneNumber).matches();
        }
    }

    public void showAlertDialog(int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

}
