package edu.nwmissouri.s521699.northwestacmadmin;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import edu.nwmissouri.s521699.northwestacmadmin.dialogs.DatePickerDialogFragment;
import edu.nwmissouri.s521699.northwestacmadmin.dialogs.TimePickerDialogFragment;
import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.UpdateEventTask;

public class EventDetailsActivity extends AppCompatActivity implements DatePickerDialogFragment.DateCallback, TimePickerDialogFragment.TimeCallback {

    private EventsList.Event event;

    private EditText eventNameET;
    private EditText eventDescriptionET;
    private EditText eventLocationET;
    private TextView eventDateTV;
    private TextView eventTimeTV;

    private CheckBox programmingContestCB;
    private CheckBox webDesignCB;
    private CheckBox openSourceCB;
    private CheckBox gameDevelopmentCB;
    private CheckBox dataScienceCB;
    private CheckBox androidCB;
    private CheckBox otherCB;

    private String oldEventName;
    private String oldEventDescription;
    private String oldEventLocation;
    private String oldEventDate;
    private String oldEventTime;
    private String oldSigGroups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        ActionBar actionBar = getSupportActionBar();
        assert getSupportActionBar() != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

//        HashMap<Integer, String> monthMap = new HashMap<>();
//        monthMap.put(0, "Jan");
//        monthMap.put(1, "Feb");
//        monthMap.put(2, "Mar");
//        monthMap.put(3, "Apr");
//        monthMap.put(4, "May");
//        monthMap.put(5, "Jun");
//        monthMap.put(6, "Jul");
//        monthMap.put(7, "Aug");
//        monthMap.put(8, "Sep");
//        monthMap.put(9, "Oct");
//        monthMap.put(10, "Nov");
//        monthMap.put(11, "Dec");

        eventNameET = (EditText) findViewById(R.id.detail_eventNameET);
        eventDescriptionET = (EditText) findViewById(R.id.detail_eventDescriptionET);
        eventLocationET = (EditText) findViewById(R.id.detail_eventLocationET);
        eventDateTV = (TextView) findViewById(R.id.detail_eventDateTV);
        eventTimeTV = (TextView) findViewById(R.id.detail_eventTimeTV);

        programmingContestCB = (CheckBox) findViewById(R.id.detail_programmingContestsCB);
        webDesignCB = (CheckBox) findViewById(R.id.detail_webDesignCB);
        openSourceCB = (CheckBox) findViewById(R.id.detail_openSourceCB);
        gameDevelopmentCB = (CheckBox) findViewById(R.id.detail_gameDevelopmentCB);
        dataScienceCB = (CheckBox) findViewById(R.id.detail_dataScienceCB);
        androidCB = (CheckBox) findViewById(R.id.detail_androidCB);
        otherCB = (CheckBox) findViewById(R.id.detail_otherCB);

        eventNameET.setEnabled(false);
        eventDescriptionET.setEnabled(false);
        eventLocationET.setEnabled(false);
        eventDateTV.setEnabled(false);
        eventTimeTV.setEnabled(false);
        programmingContestCB.setEnabled(false);
        webDesignCB.setEnabled(false);
        openSourceCB.setEnabled(false);
        gameDevelopmentCB.setEnabled(false);
        dataScienceCB.setEnabled(false);
        androidCB.setEnabled(false);
        otherCB.setEnabled(false);

        eventDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialogFragment datePickerDialogFragment = new DatePickerDialogFragment();
                datePickerDialogFragment.show(getFragmentManager(), "Tag1");
            }
        });

        eventTimeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialogFragment timePickerDialogFragment = new TimePickerDialogFragment();
                timePickerDialogFragment.show(getFragmentManager(), "Tag2");
            }
        });


        String eventName = getIntent().getStringExtra("EVENT_NAME");
        String eventDate = getIntent().getStringExtra("EVENT_DATE");

        for (EventsList.Event event : EventsList.eventsList) {
            if (event.getEventName().equals(eventName) && event.getEventDate().equals(eventDate)) {
                this.event = event;
                break;
            }
        }

        eventNameET.setText(event.getEventName());
        eventDescriptionET.setText(event.getEventDescription());
        eventLocationET.setText(event.getEventLocation());
        eventDateTV.setText(event.getEventDate());
        eventTimeTV.setText(event.getEventTime());

        for (String sigGroup : event.getSigGroups()) {
            switch (sigGroup) {
                case "P":
                    programmingContestCB.toggle();
                    break;
                case "W":
                    webDesignCB.toggle();
                    break;
                case "O":
                    openSourceCB.toggle();
                    break;
                case "G":
                    gameDevelopmentCB.toggle();
                    break;
                case "D":
                    dataScienceCB.toggle();
                    break;
                case "A":
                    androidCB.toggle();
                    break;
                case "T":
                    otherCB.toggle();
                    break;
            }
        }

        oldEventName = eventNameET.getText().toString();
        oldEventDescription = eventDescriptionET.getText().toString();
        oldEventLocation = eventLocationET.getText().toString();
        oldEventDate = eventDateTV.getText().toString();
        oldEventTime = eventTimeTV.getText().toString();
        oldSigGroups = "";

        if (programmingContestCB.isChecked())
            oldSigGroups += "P";
        if (webDesignCB.isChecked())
            oldSigGroups += "W";
        if (openSourceCB.isChecked())
            oldSigGroups += "O";
        if (gameDevelopmentCB.isChecked())
            oldSigGroups += "G";
        if (dataScienceCB.isChecked())
            oldSigGroups += "D";
        if (androidCB.isChecked())
            oldSigGroups += "A";
        if (otherCB.isChecked())
            oldSigGroups += "T";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.event_details_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.editActionBTN:
                if (item.getTitle().equals("EDIT")) {
                    item.setTitle("SAVE");

                    eventNameET.setEnabled(true);
                    eventDescriptionET.setEnabled(true);
                    eventLocationET.setEnabled(true);
                    eventDateTV.setEnabled(true);
                    eventTimeTV.setEnabled(true);
                    programmingContestCB.setEnabled(true);
                    webDesignCB.setEnabled(true);
                    openSourceCB.setEnabled(true);
                    gameDevelopmentCB.setEnabled(true);
                    dataScienceCB.setEnabled(true);
                    androidCB.setEnabled(true);
                    otherCB.setEnabled(true);

                    oldEventName = eventNameET.getText().toString();
                    oldEventDescription = eventDescriptionET.getText().toString();
                    oldEventLocation = eventLocationET.getText().toString();
                    oldEventDate = eventDateTV.getText().toString();
                    oldEventTime = eventTimeTV.getText().toString();
                    oldSigGroups = "";

                    if (programmingContestCB.isChecked())
                        oldSigGroups += "P";
                    if (webDesignCB.isChecked())
                        oldSigGroups += "W";
                    if (openSourceCB.isChecked())
                        oldSigGroups += "O";
                    if (gameDevelopmentCB.isChecked())
                        oldSigGroups += "G";
                    if (dataScienceCB.isChecked())
                        oldSigGroups += "D";
                    if (androidCB.isChecked())
                        oldSigGroups += "A";
                    if (otherCB.isChecked())
                        oldSigGroups += "T";

                } else {
                    item.setTitle("EDIT");

                    eventNameET.setEnabled(false);
                    eventDescriptionET.setEnabled(false);
                    eventLocationET.setEnabled(false);
                    eventDateTV.setEnabled(false);
                    eventTimeTV.setEnabled(false);
                    programmingContestCB.setEnabled(false);
                    webDesignCB.setEnabled(false);
                    openSourceCB.setEnabled(false);
                    gameDevelopmentCB.setEnabled(false);
                    dataScienceCB.setEnabled(false);
                    androidCB.setEnabled(false);
                    otherCB.setEnabled(false);

                    String eventName = eventNameET.getText().toString();
                    String eventDescription = eventDescriptionET.getText().toString();
                    String eventLocation = eventLocationET.getText().toString();
                    String eventDate = eventDateTV.getText().toString();
                    String eventTime = eventTimeTV.getText().toString();
                    String sigGroups = "";

                    if (programmingContestCB.isChecked())
                        sigGroups += "P";
                    if (webDesignCB.isChecked())
                        sigGroups += "W";
                    if (openSourceCB.isChecked())
                        sigGroups += "O";
                    if (gameDevelopmentCB.isChecked())
                        sigGroups += "G";
                    if (dataScienceCB.isChecked())
                        sigGroups += "D";
                    if (androidCB.isChecked())
                        sigGroups += "A";
                    if (otherCB.isChecked())
                        sigGroups += "T";

                    UpdateEventTask updateEventTask = new UpdateEventTask();
                    updateEventTask.execute(eventName, eventDescription, eventLocation, eventDate
                            , eventTime, sigGroups, oldEventName, oldEventDescription, oldEventLocation,
                            oldEventDate, oldEventTime, oldSigGroups);
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateSelect(String dateStr) {
        eventDateTV.setText(dateStr);
    }

    @Override
    public void onTimeSelect(String time) {
        eventTimeTV.setText(time);
    }
}
