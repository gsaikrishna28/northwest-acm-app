package edu.nwmissouri.s521699.northwestacmadmin;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.nwmissouri.s521699.northwestacmadmin.tasks.LoginTask;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.ResetPasswordTask;
import edu.nwmissouri.s521699.northwestacmadmin.utils.AConstants;
import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends AppCompatActivity {

    private TextView appNameLBL;
    private EditText userNameET;
    private EditText passwordET;
    private FancyButton loginBTN;
    private FancyButton resetPasswordBTN;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

        if (sharedPreferences.getBoolean(AConstants.IS_Logged_IN, false)) {
            finish();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        } else {

            if (!isOnline()) {
                Toast.makeText(this, "Connection is offline", Toast.LENGTH_LONG).show();
            }

            appNameLBL = (TextView) findViewById(R.id.appNameTV);
            userNameET = (EditText) findViewById(R.id.userNameET);
            passwordET = (EditText) findViewById(R.id.passwordET);
            loginBTN = (FancyButton) findViewById(R.id.loginBTN);
            loginBTN.setEnabled(false);
            loginBTN.getBackground().setAlpha(40);
            loginBTN.setTextColor(Color.parseColor("#55FFFFFF"));
            resetPasswordBTN = (FancyButton) findViewById(R.id.resetPasswordBTN);

            userNameET.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    if ((userNameET.getText().toString().length() > 0) && (passwordET.getText().toString().length() > 0)) {
                        loginBTN.setEnabled(true);
                        loginBTN.getBackground().setAlpha(255);
                        loginBTN.setTextColor(Color.parseColor("#FFFFFFFF"));
                    }


                    if ((userNameET.getText().toString().length() == 0) || (passwordET.getText().toString().length() == 0)) {
                        loginBTN.setEnabled(false);
                        loginBTN.getBackground().setAlpha(40);
                        loginBTN.setTextColor(Color.parseColor("#55FFFFFF"));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            passwordET.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable arg0) {
                    if ((userNameET.getText().toString().length() > 0) && (passwordET.getText().toString().length() > 0)) {
                        loginBTN.setEnabled(true);
                        loginBTN.getBackground().setAlpha(255);
                        loginBTN.setTextColor(Color.parseColor("#FFFFFFFF"));
                    }

                    if ((userNameET.getText().toString().length() == 0) || (passwordET.getText().toString().length() == 0)) {
                        loginBTN.setEnabled(false);
                        loginBTN.getBackground().setAlpha(40);
                        loginBTN.setTextColor(Color.parseColor("#55FFFFFF"));
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            loginBTN.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (!isOnline()) {
                        Toast.makeText(LoginActivity.this, "Connection is offline", Toast.LENGTH_LONG).show();
                    } else {
                        LoginTask loginTask = new LoginTask();
                        loginTask.setLoginTaskCallback(loginTaskCallback);
                        loginTask.execute(userNameET.getText().toString(), passwordET.getText().toString());
                    }
                }
            });

            resetPasswordBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isOnline()) {
                        Toast.makeText(LoginActivity.this, "Connection is offline", Toast.LENGTH_LONG).show();
                    } else {
                        ResetPasswordTask resetPasswordTask = new ResetPasswordTask();
                        resetPasswordTask.setResetPasswordCallback(resetPasswordCallback);
                        resetPasswordTask.execute();
                    }
                }
            });
        }

        if (!sharedPreferences.getBoolean(AConstants.IS_Logged_IN, false)) {
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(appNameLBL, "y", appNameLBL.getY(), appNameLBL.getY() + 200.0f);
            objectAnimator.setDuration(1500);

            ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(appNameLBL, "scaleX", appNameLBL.getScaleX(), appNameLBL.getScaleX() * 1.5f);
            objectAnimator1.setDuration(1500);

            ObjectAnimator objectAnimator2 = ObjectAnimator.ofFloat(appNameLBL, "scaleY", appNameLBL.getScaleY(), appNameLBL.getScaleY() * 1.5f);
            objectAnimator2.setDuration(1500);

            ObjectAnimator objectAnimator3 = ObjectAnimator.ofFloat(appNameLBL, "alpha", appNameLBL.getAlpha(), appNameLBL.getAlpha() + 1);
            objectAnimator2.setDuration(1500);

            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(objectAnimator, objectAnimator1, objectAnimator2, objectAnimator3);
            animatorSet.start();
        }
    }

    LoginTask.LoginTaskCallback loginTaskCallback = new LoginTask.LoginTaskCallback() {
        @Override
        public void loginSuccessFull() {
            finish();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(AConstants.IS_Logged_IN, true);
            edit.apply();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }

        @Override
        public void loginFailed() {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setTitle("Login failed");
            builder.setMessage("The username/password you entered are incorrect.\nPlease try again.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
    };

    ResetPasswordTask.ResetPasswordCallback resetPasswordCallback = new ResetPasswordTask.ResetPasswordCallback() {
        @Override
        public void onEmailSend() {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("An email was successfully sent with reset instructions.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }

        @Override
        public void resetPasswordFailed() {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("Reset password failed, try after sometime.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }
    };

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

}
