package edu.nwmissouri.s521699.northwestacmadmin;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by SaiKrishna on 10/26/2015.
 */
public class ParseApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "ZH60hfq1OfMCKzrRfOMPwi7FWi1vU4x1dZKduipj", "JkmRE9OaCVofa5Lg0HbkDdZvJ98RY5GssgUhHWVC");
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

}