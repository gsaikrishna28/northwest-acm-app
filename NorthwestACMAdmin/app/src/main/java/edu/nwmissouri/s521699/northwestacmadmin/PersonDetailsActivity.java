package edu.nwmissouri.s521699.northwestacmadmin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import edu.nwmissouri.s521699.northwestacmadmin.tasks.UpdatePersonTask;
import edu.nwmissouri.s521699.northwestacmadmin.utils.AConstants;

public class PersonDetailsActivity extends AppCompatActivity {

    private EditText personNameET;
    private EditText personRoleET;
    private EditText personPhoneNumberET;
    private EditText personEmailET;

    private String personName;
    private String personRole;
    private String personPhoneNumber;
    private String personEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_details);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        personNameET = (EditText) findViewById(R.id.detail_personNameET);
        personRoleET = (EditText) findViewById(R.id.detail_roleET);
        personPhoneNumberET = (EditText) findViewById(R.id.detail_phoneNumberET);
        personEmailET = (EditText) findViewById(R.id.detail_emailET);

        personNameET.setEnabled(false);
        personRoleET.setEnabled(false);
        personPhoneNumberET.setEnabled(false);
        personEmailET.setEnabled(false);

        personName = getIntent().getStringExtra(AConstants.PERSON_NAME);
        personRole = getIntent().getStringExtra(AConstants.PERSON_ROLE);
        personPhoneNumber = getIntent().getStringExtra(AConstants.PERSON_PHONE_NUMBER);
        personEmail = getIntent().getStringExtra(AConstants.PERSON_EMAIL);

        personNameET.setText(personName);
        personRoleET.setText(personRole);
        personPhoneNumberET.setText(personPhoneNumber);
        personEmailET.setText(personEmail);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.person_details_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.personEditActionBTNBTN:
                if (item.getTitle().equals("EDIT")) {
                    item.setTitle("SAVE");
                    personNameET.setEnabled(true);
                    personNameET.setSelection(personNameET.getText().length());
                    personRoleET.setEnabled(true);
                    personPhoneNumberET.setEnabled(true);
                    personEmailET.setEnabled(true);
                } else {
                    item.setTitle("EDIT");

                    UpdatePersonTask updatePersonTask = new UpdatePersonTask();
                    updatePersonTask.setUpdatePersonTaskCallback(updatePersonTaskCallback);
                    updatePersonTask.execute(personName, personRole, personPhoneNumber, personEmail,
                            personNameET.getText().toString(), personRoleET.getText().toString(),
                            personPhoneNumberET.getText().toString(),
                            personEmailET.getText().toString());

                    personNameET.setEnabled(false);
                    personRoleET.setEnabled(false);
                    personPhoneNumberET.setEnabled(false);
                    personEmailET.setEnabled(false);

                }
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    UpdatePersonTask.UpdatePersonTaskCallback updatePersonTaskCallback = new UpdatePersonTask.UpdatePersonTaskCallback() {
        @Override
        public void onUpdated() {
            Toast.makeText(PersonDetailsActivity.this, "Contact updated", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onUpdateFailed() {

        }
    };
}
