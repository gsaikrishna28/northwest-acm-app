package edu.nwmissouri.s521699.northwestacmadmin.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import edu.nwmissouri.s521699.northwestacmadmin.R;
import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;
import edu.nwmissouri.s521699.northwestacmadmin.fragments.EventsListFragment;

public class EventsAdapter extends ArrayAdapter<EventsList.Event> {


    public EventsAdapter(Context context, int resource, int textViewResourceId, List<EventsList.Event> objects) {

        super(context, resource, textViewResourceId, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position >= 0) {
            View view = super.getView(position, convertView, parent);

            TextView eventNameTV = (TextView) view.findViewById(R.id.listItemEventNameTV);
            TextView locationTV = (TextView) view.findViewById(R.id.listItemLocationTV);
            TextView dateTV = (TextView) view.findViewById(R.id.listItemDateTV);
            TextView initialLetterTV = (TextView) view.findViewById(R.id.initialLetterTV);
            TextView timeTV = (TextView) view.findViewById(R.id.listItemTimeTV);

            ImageView selectEventIV = (ImageView) view.findViewById(R.id.eventSelectIV);

            eventNameTV.setText(getItem(position).getEventName());
            locationTV.setText(getItem(position).getEventLocation());
            dateTV.setText(getItem(position).getEventDate());
            timeTV.setText(getItem(position).getEventTime());

            initialLetterTV.setText(String.format("%s", getItem(position).getEventName().toUpperCase().charAt(0)));

            char c = getItem(position).getEventName().toUpperCase().charAt(0);
            int initialLetterASCII = (int) c;
            if (initialLetterASCII < 69) {
                initialLetterTV.setBackgroundColor(Color.parseColor("#135995"));
            } else if (initialLetterASCII > 68 && initialLetterASCII < 73) {
                initialLetterTV.setBackgroundColor(Color.parseColor("#E1711F"));
            } else if (initialLetterASCII > 72 && initialLetterASCII < 77) {
                initialLetterTV.setBackgroundColor(Color.parseColor("#FBBC05"));
            } else if (initialLetterASCII > 76 && initialLetterASCII < 81) {
                initialLetterTV.setBackgroundColor(Color.parseColor("#EA4335"));
            } else if (initialLetterASCII > 80 && initialLetterASCII < 85) {
                initialLetterTV.setBackgroundColor(Color.parseColor("#34A953"));
            } else {
                initialLetterTV.setBackgroundColor(Color.parseColor("#C740B6"));
            }

            if (EventsListFragment.selectedEvents.contains(getItem(position))) {
                selectEventIV.setVisibility(View.VISIBLE);
            } else {
                selectEventIV.setVisibility(View.INVISIBLE);
            }
            return view;
        } else {
            return null;
        }

    }
}
