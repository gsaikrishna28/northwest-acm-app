package edu.nwmissouri.s521699.northwestacmadmin.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.nwmissouri.s521699.northwestacmadmin.R;
import edu.nwmissouri.s521699.northwestacmadmin.fragments.ContactListFragment;
import edu.nwmissouri.s521699.northwestacmadmin.persons.PersonsList;

/**
 * Created by SaiKrishna on 11/5/2015.
 */
public class PersonsAdapter extends ArrayAdapter<PersonsList.Person> {

    public PersonsAdapter(Context context, int resource, int textViewResourceId, List<PersonsList.Person> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  super.getView(position, convertView, parent);

        TextView personNameTV = (TextView)view.findViewById(R.id.listItemPersonNameTV);
        TextView personRoleTV = (TextView)view.findViewById(R.id.listItemPersonRoleTV);
        TextView personEmailTV = (TextView)view.findViewById(R.id.listItemPersonEmailTV);
        TextView personPhoneNumTV = (TextView)view.findViewById(R.id.listItemPersonPhoneNumberTV);

        ImageView selectContactIV = (ImageView) view.findViewById(R.id.contactSelectIV);

        personNameTV.setText(getItem(position).getPersonName());
        personRoleTV.setText(String.format("- %s", getItem(position).getPersonRole()));
        personEmailTV.setText(getItem(position).getPersonEmail());
        personPhoneNumTV.setText(getItem(position).getPersonPhoneNum());

        if (ContactListFragment.selectedContacts.contains(getItem(position))) {
            selectContactIV.setVisibility(View.VISIBLE);
        } else {
            selectContactIV.setVisibility(View.INVISIBLE);
        }

        return view;
    }

}
