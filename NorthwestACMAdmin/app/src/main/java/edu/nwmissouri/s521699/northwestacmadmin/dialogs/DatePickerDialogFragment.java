package edu.nwmissouri.s521699.northwestacmadmin.dialogs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.HashMap;


/**
 * Created by SaiKrishna on 10/24/2015.
 */
public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    DateCallback dateCallback;
    HashMap<Integer, String> monthMap;

    public DatePickerDialogFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.dateCallback = (DateCallback) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        this.monthMap = new HashMap<>();
        monthMap.put(0, "Jan");
        monthMap.put(1, "Feb");
        monthMap.put(2, "Mar");
        monthMap.put(3, "Apr");
        monthMap.put(4, "May");
        monthMap.put(5, "Jun");
        monthMap.put(6, "Jul");
        monthMap.put(7, "Aug");
        monthMap.put(8, "Sep");
        monthMap.put(9, "Oct");
        monthMap.put(10, "Nov");
        monthMap.put(11, "Dec");

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        dateCallback.onDateSelect((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
    }

    public interface DateCallback {
        void onDateSelect(String dateStr);
    }
}
