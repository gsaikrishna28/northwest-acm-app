package edu.nwmissouri.s521699.northwestacmadmin.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by SaiKrishna on 10/24/2015.
 */
public class TimePickerDialogFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    TimeCallback timeCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.timeCallback = (TimeCallback)activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        String min = String.format("%02d", minute);
        if (hourOfDay > 12) {
            timeCallback.onTimeSelect(hourOfDay - 12 + ":" + min + " PM");
        } else {
            timeCallback.onTimeSelect(hourOfDay + ":" + min + " AM");
        }
    }

    public interface TimeCallback {
        void onTimeSelect(String time);
    }
}
