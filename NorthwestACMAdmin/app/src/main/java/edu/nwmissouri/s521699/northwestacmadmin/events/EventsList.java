package edu.nwmissouri.s521699.northwestacmadmin.events;

import java.util.ArrayList;
import java.util.List;

public class EventsList {

    public static List<Event> eventsList = new ArrayList<>();

    public static class Event {
        private String eventName;
        private String eventDescription;
        private String eventLocation;
        private String eventDate;
        private String eventTime;
        private boolean selected;
        private ArrayList<String> sigGroups;

        public Event(String eventName, String eventDescription, String eventLocation, String eventDate, String eventTime, ArrayList<String> sigGroups) {

            this.eventName = eventName;
            this.eventDescription = eventDescription;
            this.eventLocation = eventLocation;
            this.eventDate = eventDate;
            this.eventTime = eventTime;
            this.sigGroups = sigGroups;
            this.selected = false;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public boolean isSelected() {
            return selected;
        }

        public String getEventName() {
            return eventName;
        }

        public String getEventDescription() {
            return eventDescription;
        }

        public String getEventLocation() {
            return eventLocation;
        }

        public String getEventDate() {
            return eventDate;
        }

        public String getEventTime() {
            return eventTime;
        }

        public ArrayList<String> getSigGroups() {
            return sigGroups;
        }
    }
}
