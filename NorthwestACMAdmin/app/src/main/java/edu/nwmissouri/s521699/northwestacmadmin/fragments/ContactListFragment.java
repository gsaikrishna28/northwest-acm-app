package edu.nwmissouri.s521699.northwestacmadmin.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import edu.nwmissouri.s521699.northwestacmadmin.PersonDetailsActivity;
import edu.nwmissouri.s521699.northwestacmadmin.R;
import edu.nwmissouri.s521699.northwestacmadmin.adapters.PersonsAdapter;
import edu.nwmissouri.s521699.northwestacmadmin.persons.PersonsList;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.DeleteSelectedContactsTask;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.RetrievePersonsTask;
import edu.nwmissouri.s521699.northwestacmadmin.utils.AConstants;


public class ContactListFragment extends ListFragment {

    public static ArrayList<PersonsList.Person> selectedContacts;
    int selectedCount = 0;
    boolean didClickDelete;
    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        selectedContacts = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_help, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.help_progressBar);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        progressBar.setMax(10);
        progressBar.setVisibility(View.VISIBLE);
        RetrievePersonsTask retrievePersonsTask = new RetrievePersonsTask();
        retrievePersonsTask.setPersonsTaskCallback(personsTaskCallback);
        retrievePersonsTask.execute();

        setListAdapter(new PersonsAdapter(getContext(), R.layout.persons_list_item, R.id.listItemPersonNameTV, PersonsList.personsList));

        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        getListView().setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                     @Override
                                                     public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                                                         PersonsList.Person person = PersonsList.personsList.get(position);
                                                         if (!checked) {
                                                             selectedContacts.remove(person);
                                                             selectedCount--;
                                                         } else {
                                                             selectedContacts.add(person);
                                                             selectedCount++;
                                                         }
                                                         mode.setTitle(selectedCount + " Selected");
                                                         ((PersonsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                     }

                                                     @Override
                                                     public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                         mode.setTitle("");

                                                         MenuInflater menuInflater = mode.getMenuInflater();
                                                         menuInflater.inflate(R.menu.contextual_main_menu, menu);
                                                         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                             getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                                                         }
                                                         didClickDelete = false;
                                                         return true;
                                                     }

                                                     @Override
                                                     public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                         return false;
                                                     }

                                                     @Override
                                                     public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {

                                                         switch (item.getItemId()) {
                                                             case R.id.delete_action:

                                                                 AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                                 builder.setMessage("Are you sure you want to delete?");

                                                                 builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(DialogInterface dialog, int which) {
                                                                         didClickDelete = true;

                                                                         DeleteSelectedContactsTask deleteSelectedContactsTask = new DeleteSelectedContactsTask();
                                                                         deleteSelectedContactsTask.setDeleteSelectedContactsCallback(new DeleteSelectedContactsTask.DeleteSelectedContactsCallback() {
                                                                             @Override
                                                                             public void removeSelectedContact(PersonsList.Person person) {
                                                                                 if (selectedContacts.contains(person)) {
                                                                                     selectedContacts.remove(person);
                                                                                 }
                                                                             }
                                                                         });
                                                                         deleteSelectedContactsTask.execute();
                                                                         for (PersonsList.Person person : selectedContacts) {
                                                                             PersonsList.personsList.remove(person);
                                                                         }
                                                                         ((PersonsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                                         mode.finish();
                                                                         selectedCount = 0;

                                                                     }
                                                                 });
                                                                 builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(DialogInterface dialog, int which) {

                                                                     }
                                                                 });
                                                                 builder.show();
                                                                 break;
                                                             default:
                                                                 return false;
                                                         }
                                                         return true;
                                                     }

                                                     @Override
                                                     public void onDestroyActionMode(ActionMode mode) {
                                                         if (!didClickDelete) {
                                                             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                                 getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                                                             }
                                                             selectedContacts.clear();
                                                             ((PersonsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                             selectedCount = 0;
                                                         }
                                                     }
                                                 }
        );
    }


    RetrievePersonsTask.PersonsTaskCallback personsTaskCallback = new RetrievePersonsTask.PersonsTaskCallback() {
        @Override
        public void updatePersonsList() {
            setListAdapter(new PersonsAdapter(getContext(), R.layout.persons_list_item, R.id.listItemPersonNameTV, PersonsList.personsList));
        }

        @Override
        public void onFinishRetrievingPersons() {
            progressBar.setVisibility(View.GONE);
        }
    };


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Intent intent = new Intent(getContext(), PersonDetailsActivity.class);
        intent.putExtra(AConstants.PERSON_NAME, PersonsList.personsList.get(position).getPersonName());
        intent.putExtra(AConstants.PERSON_ROLE, PersonsList.personsList.get(position).getPersonRole());
        intent.putExtra(AConstants.PERSON_PHONE_NUMBER, PersonsList.personsList.get(position).getPersonPhoneNum());
        intent.putExtra(AConstants.PERSON_EMAIL, PersonsList.personsList.get(position).getPersonEmail());

        startActivityForResult(intent, AConstants.PERSON_DETAILS_REQUEST_CODE);
    }

}

