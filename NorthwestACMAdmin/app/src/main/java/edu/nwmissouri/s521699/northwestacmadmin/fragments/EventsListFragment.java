package edu.nwmissouri.s521699.northwestacmadmin.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import edu.nwmissouri.s521699.northwestacmadmin.EventDetailsActivity;
import edu.nwmissouri.s521699.northwestacmadmin.R;
import edu.nwmissouri.s521699.northwestacmadmin.adapters.EventsAdapter;
import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.DeleteSelectedEvents;
import edu.nwmissouri.s521699.northwestacmadmin.tasks.RetrieveEventsTasks;


public class EventsListFragment extends ListFragment {

    public static ArrayList<EventsList.Event> selectedEvents;
    int selectedCount = 0;
    boolean didClickDelete;
    private ProgressBar progressBar;

    public EventsListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        selectedEvents = new ArrayList<>();

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.main_progressBar);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setMax(10);
        RetrieveEventsTasks retrieveEventsTasks = new RetrieveEventsTasks();
        retrieveEventsTasks.setEventTaskCallback(eventTaskCallback);
        retrieveEventsTasks.setContext(getContext());
        retrieveEventsTasks.execute();

        setListAdapter(new EventsAdapter(getContext(), R.layout.events_list_item, R.id.listItemEventNameTV, EventsList.eventsList));

        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        getListView().setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                     @Override
                                                     public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                                                         EventsList.Event event = EventsList.eventsList.get(position);
                                                         if (!checked) {
                                                             selectedEvents.remove(event);
                                                             selectedCount--;
                                                         } else {
                                                             selectedEvents.add(event);
                                                             selectedCount++;
                                                         }
                                                         mode.setTitle(selectedCount + " Selected");
                                                         ((EventsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                     }

                                                     @Override
                                                     public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                         mode.setTitle("");
                                                         MenuInflater menuInflater = mode.getMenuInflater();
                                                         menuInflater.inflate(R.menu.contextual_main_menu, menu);

                                                         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                             getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.context_statusBar_color));
                                                         }
                                                         didClickDelete = false;
                                                         return true;
                                                     }

                                                     @Override
                                                     public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                         return false;
                                                     }

                                                     @Override
                                                     public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {

                                                         switch (item.getItemId()) {
                                                             case R.id.delete_action:
                                                                 AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                                 builder.setMessage("Are you sure you want to delete?");
                                                                 builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(DialogInterface dialog, int which) {
                                                                         didClickDelete = true;
                                                                         DeleteSelectedEvents deleteSelectedEvents = new DeleteSelectedEvents();
                                                                         deleteSelectedEvents.setDeleteCallback(deleteCallback);
                                                                         deleteSelectedEvents.execute();
                                                                         for (EventsList.Event event : selectedEvents) {
                                                                             EventsList.eventsList.remove(event);
                                                                         }
                                                                         ((EventsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                                         mode.finish();
                                                                         selectedCount = 0;
                                                                     }
                                                                 });
                                                                 builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(DialogInterface dialog, int which) {

                                                                     }
                                                                 });
                                                                 builder.show();

                                                                 break;
                                                             default:
                                                                 return false;
                                                         }
                                                         return true;
                                                     }

                                                     @Override
                                                     public void onDestroyActionMode(ActionMode mode) {
                                                         if (!didClickDelete) {

                                                             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                                 getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
                                                             }
                                                             selectedEvents.clear();
                                                             ((EventsAdapter) getListView().getAdapter()).notifyDataSetChanged();
                                                             selectedCount = 0;
                                                         }
                                                     }
                                                 }
        );
    }

    DeleteSelectedEvents.DeleteCallback deleteCallback = new DeleteSelectedEvents.DeleteCallback() {
        @Override
        public void removeSelectedEvent(EventsList.Event event) {
            if (selectedEvents.contains(event)) {
                selectedEvents.remove(event);
            }
        }
    };

    RetrieveEventsTasks.EventTaskCallback eventTaskCallback = new RetrieveEventsTasks.EventTaskCallback() {
        @Override
        public void updateEventListView() {
            setListAdapter(new EventsAdapter(getContext(), R.layout.events_list_item, R.id.listItemEventNameTV, EventsList.eventsList));
        }

        @Override
        public void onFinishRetrievingEvents() {
            progressBar.setVisibility(View.GONE);
        }

    };

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        v.setSelected(true);

        Intent detailIntent = new Intent(getContext(), EventDetailsActivity.class);
        detailIntent.putExtra("EVENT_NAME", EventsList.eventsList.get(position).getEventName());
        detailIntent.putExtra("EVENT_DATE", EventsList.eventsList.get(position).getEventDate());
        startActivity(detailIntent);

    }
}
