package edu.nwmissouri.s521699.northwestacmadmin.persons;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SaiKrishna on 11/5/2015.
 */
public class PersonsList {
    public static List<Person> personsList = new ArrayList<>();

    public static class Person {
        private String personName;
        private String personRole;
        private String personPhoneNum;
        private String personEmail;

        public Person(String personName, String personRole, String personPhoneNum, String personEmail) {
            this.personName = personName;
            this.personRole = personRole;
            this.personPhoneNum = personPhoneNum;
            this.personEmail = personEmail;
        }

        public String getPersonName() {
            return personName;
        }

        public String getPersonRole() {
            return personRole;
        }

        public String getPersonPhoneNum() {
            return personPhoneNum;
        }

        public String getPersonEmail() {
            return personEmail;
        }

        public void setPersonName(String personName) {
            this.personName = personName;
        }

        public void setPersonRole(String personRole) {
            this.personRole = personRole;
        }

        public void setPersonPhoneNum(String personPhoneNum) {
            this.personPhoneNum = personPhoneNum;
        }

        public void setPersonEmail(String personEmail) {
            this.personEmail = personEmail;
        }
    }
}
