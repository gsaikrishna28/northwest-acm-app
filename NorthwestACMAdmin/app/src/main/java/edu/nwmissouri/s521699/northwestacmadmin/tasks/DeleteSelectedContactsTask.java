package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import edu.nwmissouri.s521699.northwestacmadmin.fragments.ContactListFragment;
import edu.nwmissouri.s521699.northwestacmadmin.persons.PersonsList;


public class DeleteSelectedContactsTask extends AsyncTask<Void, Void, Void> {

    PersonsList.Person currentContact;
    private DeleteSelectedContactsCallback deleteSelectedContactsCallback;

    public void setDeleteSelectedContactsCallback(DeleteSelectedContactsCallback deleteSelectedContactsCallback) {
        this.deleteSelectedContactsCallback = deleteSelectedContactsCallback;
    }

    @Override
    protected Void doInBackground(Void... params) {

        for (PersonsList.Person person : ContactListFragment.selectedContacts) {
            currentContact = person;
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Persons");
            query.whereEqualTo("personName", person.getPersonName());
            query.whereEqualTo("personRole", person.getPersonRole());
            query.whereEqualTo("personPhoneNum", person.getPersonPhoneNum());
            query.whereEqualTo("personEmail", person.getPersonEmail());

            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(final ParseObject contactObject, ParseException e) {
                    if ( contactObject != null) {
                        contactObject.deleteInBackground(new com.parse.DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                deleteSelectedContactsCallback.removeSelectedContact(currentContact);
                            }
                        });
                    }
                }
            });
        }
        return null;
    }

    public interface DeleteSelectedContactsCallback {
        void removeSelectedContact(PersonsList.Person person);
    }

}

