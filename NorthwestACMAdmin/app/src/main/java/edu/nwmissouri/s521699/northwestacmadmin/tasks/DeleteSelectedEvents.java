package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;
import edu.nwmissouri.s521699.northwestacmadmin.fragments.EventsListFragment;


public class DeleteSelectedEvents extends AsyncTask<Void, Void, Void> {

    DeleteCallback deleteCallback;

    public void setDeleteCallback(DeleteCallback deleteCallback) {
        this.deleteCallback = deleteCallback;
    }

    EventsList.Event currentEvent;

    @Override
    protected Void doInBackground(Void... params) {
        for (EventsList.Event event : EventsListFragment.selectedEvents) {
            currentEvent = event;
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Events");
            query.whereEqualTo("eventName", event.getEventName());
            query.whereEqualTo("eventDescription", event.getEventDescription());
            query.whereEqualTo("eventLocation", event.getEventLocation());
            query.whereEqualTo("eventDate", event.getEventDate());
            query.whereEqualTo("eventTime", event.getEventTime());
            if (event.getSigGroups().size() > 0) {
                query.whereContainsAll("sigGroups", event.getSigGroups());
            }
            query.getFirstInBackground(new GetCallback<ParseObject>() {
                public void done(final ParseObject eventObject, ParseException e) {
                    if (eventObject != null) {
                        eventObject.deleteInBackground(new com.parse.DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                deleteCallback.removeSelectedEvent(currentEvent);
                            }
                        });
                    }
                }
            });
        }
        return null;
    }

    public interface DeleteCallback {
        void removeSelectedEvent(EventsList.Event event);
    }

}
