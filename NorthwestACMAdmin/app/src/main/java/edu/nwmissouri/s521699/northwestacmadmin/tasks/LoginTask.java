package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;


import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


/**
 * Created by SaiKrishna on 11/21/2015.
 */
public class LoginTask extends AsyncTask<String, Void, Void> {

    LoginTaskCallback loginTaskCallback;

    public void setLoginTaskCallback(LoginTaskCallback loginTaskCallback) {
        this.loginTaskCallback = loginTaskCallback;
    }

    @Override
    protected Void doInBackground(final String... params) {

        final ParseQuery<ParseObject> query = ParseQuery.getQuery("_User");
        query.whereEqualTo("email", params[0]);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (object != null) {
                    login(object.getString("username"), params[1]);
                } else {
                    login(params[0], params[1]);
                }
            }
        });
        return null;
    }

    void login(String userName, String password) {
        ParseUser.logInInBackground(userName, password, new LogInCallback() {
            public void done(final ParseUser user, ParseException e) {
                if (user != null) {
                    loginTaskCallback.loginSuccessFull();
                } else {
                    loginTaskCallback.loginFailed();
                }
            }
        });
    }

    public interface LoginTaskCallback {
        void loginSuccessFull();
        void loginFailed();
    }
}
