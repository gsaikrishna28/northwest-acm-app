package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

/**
 * Created by SaiKrishna on 12/2/2015.
 */
public class ResetPasswordTask extends AsyncTask<Void, Void, Void> {

    ResetPasswordCallback resetPasswordCallback;

    public void setResetPasswordCallback(ResetPasswordCallback resetPasswordCallback) {
        this.resetPasswordCallback = resetPasswordCallback;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ParseUser.requestPasswordResetInBackground("acm@nwmissouri.edu", new RequestPasswordResetCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    resetPasswordCallback.onEmailSend();
                } else {
                    resetPasswordCallback.resetPasswordFailed();
                    // Something went wrong. Look at the ParseException to see what's up.
                }
            }
        });
        return null;
    }

    public interface ResetPasswordCallback {
        void onEmailSend();
        void resetPasswordFailed();
    }
}
