package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;

public class RetrieveEventsTasks extends AsyncTask<Void, Void, Void> {

    private EventTaskCallback eventTaskCallback;
    private Context context;

    public void setEventTaskCallback(EventTaskCallback eventTaskCallback) {
        this.eventTaskCallback = eventTaskCallback;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        EventsList.eventsList.clear();

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Events");
        query.orderByDescending("sortingDate");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventsList, ParseException e) {
                if (e == null) {
                    if (eventsList.size() > 0) {
                        boolean eventRetrieved = false;
                        for (ParseObject event : eventsList) {
                            String eventName = event.getString("eventName");
                            String eventDescription = event.getString("eventDescription");
                            String eventLocation = event.getString("eventLocation");
                            String eventDate = event.getString("eventDate");
                            String eventTime = event.getString("eventTime");
                            ArrayList<String> sigGroups = new ArrayList<String>();
                            sigGroups = (ArrayList<String>) event.get("sigGroups");
                            if (EventsList.eventsList != null && EventsList.eventsList.size() > 0) {
                                for (EventsList.Event ev : EventsList.eventsList) {
                                    if (eventName.equals(ev.getEventName()) && eventDescription.equals(ev.getEventDescription())) {
                                        eventRetrieved = true;
                                        break;
                                    }
                                }
                            }

                            if (!eventRetrieved) {
                                EventsList.Event newEvent = new EventsList.Event(eventName, eventDescription, eventLocation, eventDate, eventTime, sigGroups);
                                EventsList.eventsList.add(newEvent);
                                eventTaskCallback.updateEventListView();
                            }

                            eventRetrieved = false;
                        }
                    }
                }
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        eventTaskCallback.onFinishRetrievingEvents();
    }

    public interface EventTaskCallback {
        void updateEventListView();
        void onFinishRetrievingEvents();
    }
}
