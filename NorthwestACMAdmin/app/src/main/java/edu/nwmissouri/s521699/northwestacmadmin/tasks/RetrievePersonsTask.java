package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import edu.nwmissouri.s521699.northwestacmadmin.persons.PersonsList;

/**
 * Created by SaiKrishna on 11/5/2015.
 */
public class RetrievePersonsTask extends AsyncTask<Void, Void, Void> {

    private PersonsTaskCallback personsTaskCallback;

    public void setPersonsTaskCallback(PersonsTaskCallback personsTaskCallback) {
        this.personsTaskCallback = personsTaskCallback;
    }

    @Override
    protected Void doInBackground(Void... params) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Persons");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> eventsList, ParseException e) {
                if (e == null) {
                    if(eventsList.size() > 0) {
                        boolean personRetrieved = false;
                        for (ParseObject event:eventsList) {
                            String personName = event.getString("personName");
                            String personRole = event.getString("personRole");
                            String personPhoneNumber = event.getString("personPhoneNum");
                            String personEmail = event.getString("personEmail");

                            for (PersonsList.Person person : PersonsList.personsList) {
                                if (personName.equals(person.getPersonName()) && personRole.equals(person.getPersonRole())) {
                                    personRetrieved = true;
                                    break;
                                }
                            }

//                            if(events_db.isEventStored(eventName, eventDescription, eventLocation, eventDate, eventTime)) {
//                                eventRetrieved = true;
//                                //eventTaskCallback.updateEventListView();
//                            }

                            if (!personRetrieved) {
                                PersonsList.Person newPerson = new PersonsList.Person(personName, personRole, personPhoneNumber, personEmail);
                                //events_db.insertEvent(eventName, eventDescription, eventLocation, eventDate, eventTime);
                                PersonsList.personsList.add(newPerson);
                                personsTaskCallback.updatePersonsList();
                            }

                            personRetrieved = false;
                        }
                    }
                }
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        personsTaskCallback.onFinishRetrievingPersons();
    }

    public interface PersonsTaskCallback {
        void updatePersonsList();
        void onFinishRetrievingPersons();
    }
}
