package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;

import edu.nwmissouri.s521699.northwestacmadmin.events.EventsList;

public class UpdateEventTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... params) {

        final String eventName = params[0];
        final String eventDescription = params[1];
        final String eventLocation = params[2];
        final String eventDate = params[3];
        final String eventTime = params[4];
        final String sigGroups = params[5];

        final ArrayList<String> sigGroupsArrayList = new ArrayList<>();

        for (int i = 0; i < sigGroups.length(); i++) {
            sigGroupsArrayList.add(String.valueOf(sigGroups.charAt(i)));
        }

        String oldEventName = params[6];
        String oldEventDescription = params[7];
        String oldEventLocation = params[8];
        String oldEventDate = params[9];
        String oldEventTime = params[10];
        String oldSigGroups = params[11];

        final ArrayList<String> oldSigGroupsArrayList = new ArrayList<>();

        for (int i = 0; i < oldSigGroups.length(); i++) {
            oldSigGroupsArrayList.add(String.valueOf(oldSigGroups.charAt(i)));
        }

        for (EventsList.Event event : EventsList.eventsList) {
            if (event.getEventName().equals(oldEventName) && event.getEventDate().equals(oldEventDate)
                    && event.getEventLocation().equals(oldEventLocation)) {
                EventsList.eventsList.remove(event);
                break;
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Events");
        query.whereEqualTo("eventName", oldEventName);
        query.whereEqualTo("eventDescription", oldEventDescription);
        query.whereEqualTo("eventLocation", oldEventLocation);
        query.whereEqualTo("eventDate", oldEventDate);
        query.whereEqualTo("eventTime", oldEventTime);
        query.whereContainsAll("sigGroups", oldSigGroupsArrayList);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            public void done(final ParseObject eventObject, ParseException e) {
                if (eventObject != null) {
                    eventObject.put("eventName", eventName);
                    eventObject.put("eventDescription", eventDescription);
                    eventObject.put("eventLocation", eventLocation);
                    eventObject.put("eventDate", eventDate);
                    eventObject.put("eventTime", eventTime);
                    eventObject.removeAll("sigGroups", oldSigGroupsArrayList);
                    eventObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            eventObject.addAllUnique("sigGroups", sigGroupsArrayList);
                            eventObject.saveInBackground();
                        }
                    });

                }
            }
        });

        return null;
    }
}
