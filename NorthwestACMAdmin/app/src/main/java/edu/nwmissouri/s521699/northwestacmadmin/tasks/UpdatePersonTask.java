package edu.nwmissouri.s521699.northwestacmadmin.tasks;

import android.os.AsyncTask;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import edu.nwmissouri.s521699.northwestacmadmin.persons.PersonsList;

public class UpdatePersonTask extends AsyncTask<String, Void, Void> {

    PersonsList.Person currentPerson;

    private UpdatePersonTaskCallback updatePersonTaskCallback;

    public void setUpdatePersonTaskCallback(UpdatePersonTaskCallback updatePersonTaskCallback) {
        this.updatePersonTaskCallback = updatePersonTaskCallback;
    }

    @Override
    protected Void doInBackground(String... params) {
        final String oldPersonName = params[0];
        final String oldPersonRole = params[1];
        final String oldPersonPhoneNumber = params[2];
        final String oldPersonEmail = params[3];


        final String personName = params[4];
        final String personRole = params[5];
        final String personPhoneNumber = params[6];
        final String personEmail = params[7];

        for (PersonsList.Person person : PersonsList.personsList) {
            if (oldPersonName.equals(person.getPersonName()) && oldPersonRole.equals(person.getPersonRole())
                    && oldPersonPhoneNumber.equals(person.getPersonPhoneNum())
                    && oldPersonEmail.equals(person.getPersonEmail())) {
                person.setPersonName(personName);
                person.setPersonRole(personRole);
                person.setPersonPhoneNum(personPhoneNumber);
                person.setPersonEmail(personEmail);
                currentPerson = person;
            }
        }

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Persons");
        query.whereEqualTo("personName", oldPersonName);
        query.whereEqualTo("personRole", oldPersonRole);
        query.whereEqualTo("personPhoneNum", oldPersonPhoneNumber);
        query.whereEqualTo("personEmail", oldPersonEmail);

        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject personObject, ParseException e) {
                if (e == null) {
                    personObject.put("personName", personName);
                    personObject.put("personRole", personRole);
                    personObject.put("personPhoneNum", personPhoneNumber);
                    personObject.put("personEmail", personEmail);
                    personObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {

                                updatePersonTaskCallback.onUpdated();
                            } else {
                                updatePersonTaskCallback.onUpdateFailed();
                            }
                        }
                    });
                }
            }
        });

        return null;
    }

    public interface UpdatePersonTaskCallback {
        void onUpdated();

        void onUpdateFailed();
    }
}
