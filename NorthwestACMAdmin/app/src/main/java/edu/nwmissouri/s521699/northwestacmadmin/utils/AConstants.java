package edu.nwmissouri.s521699.northwestacmadmin.utils;

/**
 * Created by SaiKrishna on 11/21/2015.
 */
public class AConstants {

    public static String IS_Logged_IN = "isLoggedIn";
    public static String PERSON_NAME = "personName";
    public static String PERSON_ROLE = "personRole";
    public static String PERSON_PHONE_NUMBER = "personPhoneNumber";
    public static String PERSON_EMAIL = "personEmail";
    public static int PERSON_DETAILS_REQUEST_CODE = 13;
}
